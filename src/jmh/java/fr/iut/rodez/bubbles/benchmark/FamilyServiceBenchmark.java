package fr.iut.rodez.bubbles.benchmark;

import fr.iut.rodez.bubbles.domain.FamilyMember;
import fr.iut.rodez.bubbles.domain.Identity;
import fr.iut.rodez.bubbles.domain.Position;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Warmup;

import fr.iut.rodez.bubbles.service.FamilyService;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.MILLISECONDS)
@State(Scope.Benchmark)
public class FamilyServiceBenchmark {

	private FamilyMember alice;
	private FamilyMember bob;
	private FamilyMember charlie;
	private FamilyMember diana;
	private FamilyMember eve;

	// Ajout de générations supplémentaires
	private FamilyMember fiona;
	private FamilyMember george;
	private FamilyMember harry;
	private FamilyMember isabella;
	private FamilyMember jack;
	private FamilyMember karen;
	private FamilyMember liam;
	private FamilyMember mia;
	private FamilyMember noah;
	private FamilyMember olivia;

	@Setup
	public void setup() throws MalformedURLException {
		alice = new FamilyMember(new Identity(UUID.randomUUID(), "Alice", new URL("http://example.com/alice.png")), new Position(0, 0));
		bob = new FamilyMember(new Identity(UUID.randomUUID(), "Bob", new URL("http://example.com/bob.png")), new Position(1, 0));
		charlie = new FamilyMember(new Identity(UUID.randomUUID(), "Charlie", new URL("http://example.com/charlie.png")), new Position(2, 0));
		diana = new FamilyMember(new Identity(UUID.randomUUID(), "Diana", new URL("http://example.com/diana.png")), new Position(3, 0));
		eve = new FamilyMember(new Identity(UUID.randomUUID(), "Eve", new URL("http://example.com/eve.png")), new Position(4, 0));

		alice.parentOf(bob);
		alice.parentOf(charlie);
		bob.parentOf(diana);
		charlie.parentOf(eve);

		// Génération 2
		fiona = new FamilyMember(new Identity(UUID.randomUUID(), "Fiona", new URL("http://example.com/fiona.png")), new Position(5, 0));
		george = new FamilyMember(new Identity(UUID.randomUUID(), "George", new URL("http://example.com/george.png")), new Position(6, 0));
		diana.parentOf(fiona);
		diana.parentOf(george);

		// Génération 3
		harry = new FamilyMember(new Identity(UUID.randomUUID(), "Harry", new URL("http://example.com/harry.png")), new Position(7, 0));
		isabella = new FamilyMember(new Identity(UUID.randomUUID(), "Isabella", new URL("http://example.com/isabella.png")), new Position(8, 0));
		eve.parentOf(harry);
		eve.parentOf(isabella);

		// Génération 4
		jack = new FamilyMember(new Identity(UUID.randomUUID(), "Jack", new URL("http://example.com/jack.png")), new Position(9, 0));
		karen = new FamilyMember(new Identity(UUID.randomUUID(), "Karen", new URL("http://example.com/karen.png")), new Position(10, 0));
		fiona.parentOf(jack);
		fiona.parentOf(karen);

		// Génération 5
		liam = new FamilyMember(new Identity(UUID.randomUUID(), "Liam", new URL("http://example.com/liam.png")), new Position(11, 0));
		mia = new FamilyMember(new Identity(UUID.randomUUID(), "Mia", new URL("http://example.com/mia.png")), new Position(12, 0));
		george.parentOf(liam);
		george.parentOf(mia);

		// Génération 6
		noah = new FamilyMember(new Identity(UUID.randomUUID(), "Noah", new URL("http://example.com/noah.png")), new Position(13, 0));
		olivia = new FamilyMember(new Identity(UUID.randomUUID(), "Olivia", new URL("http://example.com/olivia.png")), new Position(14, 0));
		harry.parentOf(noah);
		harry.parentOf(olivia);
	}

	@Benchmark
	@Fork(value = 1, warmups = 1)
	@Warmup(iterations = 5, time = 1, timeUnit = TimeUnit.SECONDS)
	@Measurement(iterations = 25, time = 1, timeUnit = TimeUnit.SECONDS)
	public void benchmarkFindShortestPath() {
		FamilyService.relationBetween(alice, olivia);
	}
}
