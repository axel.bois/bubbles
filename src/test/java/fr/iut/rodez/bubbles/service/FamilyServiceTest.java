package fr.iut.rodez.bubbles.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.iut.rodez.bubbles.domain.Family;
import fr.iut.rodez.bubbles.domain.FamilyMember;
import fr.iut.rodez.bubbles.domain.Identity;
import fr.iut.rodez.bubbles.domain.Position;
import fr.iut.rodez.bubbles.service.FamilyService.FamilyLoadingResult;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

public class FamilyServiceTest {

    private ObjectMapper objectMapper;

    @BeforeEach
    public void setUp() {
        objectMapper = new ObjectMapper();
        objectMapper.setPropertyNamingStrategy(com.fasterxml.jackson.databind.PropertyNamingStrategies.SNAKE_CASE);
    }

    @Test
    public void testLoadFamilyFromFile_success() {
        // Arrange
        File file = Paths.get("src", "test", "resources", "family.json").toFile();

        // Act
        FamilyLoadingResult result = FamilyService.loadFamilyFromFile(file);

        // Assert
        assertInstanceOf(FamilyLoadingResult.Loaded.class, result);
        Family family = ((FamilyLoadingResult.Loaded) result).family();
        assertEquals("Doe", family.name());
        assertEquals(9, family.members().size());
    }

    @Test
    public void testLoadFamilyFromFile_error() {
        // Arrange
        File file = new File("nonexistent.json");

        // Act
        FamilyLoadingResult result = FamilyService.loadFamilyFromFile(file);

        // Assert
        assertTrue(result instanceof FamilyLoadingResult.LoadingError);
    }

    @Test
    public void testCreateRelation_parent() throws MalformedURLException {
        // Arrange
        UUID parentId = UUID.randomUUID();
        UUID childId = UUID.randomUUID();

        Identity parentIdentity = new Identity(parentId, "Homer", null);
        Identity childIdentity = new Identity(childId, "Bart", null);

        FamilyMember parent = new FamilyMember(parentIdentity, new Position(0, 0));
        FamilyMember child = new FamilyMember(childIdentity, new Position(0, 0));

        Set<FamilyMember> members = new HashSet<>();
        members.add(parent);
        members.add(child);

        FamilyFile.Relation relation = new FamilyFile.Relation(parentId, childId, FamilyFile.Relation.Type.PARENT);

        // Act
        FamilyService.createRelation(relation, members);

        // Assert
        assertTrue(parent.isRelated(child));
        assertEquals(parent, child.relations().iterator().next().related());
    }

    @Test
    public void testFindById() {
        // Arrange
        UUID memberId = UUID.randomUUID();
        Identity identity = new Identity(memberId, "Lisa", null);
        FamilyMember member = new FamilyMember(identity, new Position(0, 0));
        Set<FamilyMember> members = Set.of(member);

        // Act
        Optional<FamilyMember> foundMember = FamilyService.findById(members, memberId);

        // Assert
        assertTrue(foundMember.isPresent());
        assertEquals(member, foundMember.get());
    }
}
