package fr.iut.rodez.bubbles.service;

import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import com.fasterxml.jackson.databind.exc.MismatchedInputException;
import fr.iut.rodez.bubbles.domain.Family;
import fr.iut.rodez.bubbles.domain.FamilyMember;
import fr.iut.rodez.bubbles.service.FamilyService.FamilyLoadingResult;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Set;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

class FamilyFileServiceTest {

    @Test
    void shouldReturnLoadedWhenFileIsCorrectlyLoaded() {
        // Given
        var file = new File("src/test/resources/family.json");

        // When
        var result = FamilyService.loadFamilyFromFile(file);

        // Then
        FamilyLoadingResult.Loaded loaded = assertInstanceOf(FamilyLoadingResult.Loaded.class, result);

        Family family = loaded.family();
        assertEquals("Doe", family.name());

        Set<FamilyMember> members = family.members();
        assertEquals(9, members.size());
    }

    @Test
    void shouldReturnLoadingErrorWhenFileIsIncorrect() {
        // Given
        var file = new File("src/test/resources/incorrect-path.json");

        // When
        var result = FamilyService.loadFamilyFromFile(file);

        // Then
        FamilyLoadingResult.LoadingError loadingError = assertInstanceOf(FamilyLoadingResult.LoadingError.class, result);
        assertInstanceOf(IOException.class, loadingError.cause());
    }

    @Test
    void shouldReturnLoadingErrorWhenFileIsNotJson() {
        // Given
        var file = new File("src/test/resources/not-json.txt");

        // When
        var result = FamilyService.loadFamilyFromFile(file);

        // Then
        FamilyLoadingResult.LoadingError loadingError = assertInstanceOf(FamilyLoadingResult.LoadingError.class, result);
        assertInstanceOf(MismatchedInputException.class, loadingError.cause());
    }

    @Test
    void shouldReturnLoadingErrorWhenFileIsNotValidJson() {
        // Given
        var file = new File("src/test/resources/invalid.json");

        // When
        var result = FamilyService.loadFamilyFromFile(file);

        // Then
        FamilyLoadingResult.LoadingError loadingError = assertInstanceOf(FamilyLoadingResult.LoadingError.class, result);
        assertInstanceOf(MismatchedInputException.class, loadingError.cause());
    }

    @Test
    void shouldReturnLoadingErrorWhenRequiredFieldsAreMissing() {
        // Given
        File file = createTempFile("{\"invalid\":\"Doe\",\"members\":[],\"relations\":[]}");

        // When
        var result = FamilyService.loadFamilyFromFile(file);

        // Then
        FamilyLoadingResult.LoadingError loadingError = assertInstanceOf(FamilyLoadingResult.LoadingError.class, result);
        assertInstanceOf(MismatchedInputException.class, loadingError.cause());

        // Cleanup
        file.delete();
    }

    @Test
    void shouldReturnLoadingErrorWhenRelationsAreInvalid() {
        // Given
        File file = createTempFile("{\"name\":\"Doe\",\"members\":[{\"id\":\"123\",\"name\":\"John\",\"position\":{\"x\":0,\"y\":0}}],\"relations\":[{\"from\":\"123\",\"to\":\"999\",\"type\":\"PARENT\"}]}");

        // When
        var result = FamilyService.loadFamilyFromFile(file);

        // Then
        FamilyLoadingResult.LoadingError loadingError = assertInstanceOf(FamilyLoadingResult.LoadingError.class, result);
        assertInstanceOf(InvalidFormatException.class, loadingError.cause());

        // Cleanup
        file.delete();
    }

    @Test
    void shouldReturnLoadingErrorWhenMemberFieldsAreMissing() {
        // Given
        File file = createTempFile("{\"name\":\"Doe\",\"members\":[{\"id\":\"123\",\"name\":\"John\"}],\"relations\":[]}");

        // When
        var result = FamilyService.loadFamilyFromFile(file);

        // Then
        FamilyLoadingResult.LoadingError loadingError = assertInstanceOf(FamilyLoadingResult.LoadingError.class, result);
        assertInstanceOf(MismatchedInputException.class, loadingError.cause());

        // Cleanup
        file.delete();
    }

    @Test
    void shouldReturnLoadingErrorWhenMemberIdIsInvalidUUID() {
        // Given
        File file = createTempFile("{\"name\":\"Doe\",\"members\":[{\"id\":\"invalid-uuid\",\"name\":\"John\",\"position\":{\"x\":0,\"y\":0}}],\"relations\":[]}");

        // When
        var result = FamilyService.loadFamilyFromFile(file);

        // Then
        FamilyLoadingResult.LoadingError loadingError = assertInstanceOf(FamilyLoadingResult.LoadingError.class, result);
        assertInstanceOf(InvalidFormatException.class, loadingError.cause());

        // Cleanup
        file.delete();
    }

    private File createTempFile(String content) {
        try {
            File tempFile = File.createTempFile("temp", ".json");
            FileWriter writer = new FileWriter(tempFile);
            writer.write(content);
            writer.close();
            return tempFile;
        } catch (IOException e) {
            throw new RuntimeException("Failed to create temp file for testing", e);
        }
    }
}