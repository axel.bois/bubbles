package fr.iut.rodez.bubbles.utils;

import org.junit.jupiter.api.Test;

import static fr.iut.rodez.bubbles.utils.Validations.requirePositive;
import static fr.iut.rodez.bubbles.utils.Validations.requireStrictlyPositive;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class ValidationsTest {

    @Test
    void shouldThrowExceptionIfNumberIsNotStrictlyPositive() {
        assertThrows(IllegalArgumentException.class, () -> requireStrictlyPositive(-1));
        assertThrows(IllegalArgumentException.class, () -> requireStrictlyPositive(0));
    }

    @Test
    void shouldReturnTheValueIfItIsStrictlyPositive() {
        assertEquals(1, requireStrictlyPositive(1));
        assertEquals(1, requireStrictlyPositive(1, () -> "Value must be positive..."));
    }

    @Test
    void shouldThrowExceptionWithCustomMessageIfNumberIsNotStrictlyPositive() {
        assertThrows(IllegalArgumentException.class, () -> requireStrictlyPositive(-1, () -> "Value must be positive..."));
        assertThrows(IllegalArgumentException.class, () -> requireStrictlyPositive(0, () -> "Value must be positive..."));
    }

    @Test
    void shouldThrowExceptionIfDoubleIsNotPositive() {
        assertThrows(IllegalArgumentException.class, () -> requirePositive(-1.0));
        assertThrows(IllegalArgumentException.class, () -> requirePositive(-0.1));
    }

    @Test
    void shouldReturnTheValueIfDoubleIsPositive() {
        assertEquals(1.0, requirePositive(1.0));
        assertEquals(0.1, requirePositive(0.1));
    }

    @Test
    void shouldThrowExceptionWithCustomMessageIfDoubleIsNotPositive() {
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
                () -> requirePositive(-1.0, () -> "Value must be positive..."));
        assertEquals("Value must be positive...", exception.getMessage());

        exception = assertThrows(IllegalArgumentException.class,
                () -> requirePositive(-0.1, () -> "Value must be positive..."));
        assertEquals("Value must be positive...", exception.getMessage());
    }

    @Test
    void shouldThrowExceptionIfDoubleIsNaNOrInfinite() {
        assertThrows(IllegalArgumentException.class, () -> requirePositive(Double.NaN));
        assertThrows(IllegalArgumentException.class, () -> requirePositive(Double.NEGATIVE_INFINITY));
        assertThrows(IllegalArgumentException.class, () -> requirePositive(Double.POSITIVE_INFINITY));
    }

    @Test
    void shouldReturnTheValueIfDoubleIsZeroOrPositive() {
        assertEquals(0.0, requirePositive(0.0));
        assertEquals(1.0, requirePositive(1.0));
    }
}