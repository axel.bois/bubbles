module fr.iut.rodez.bubbles {
    requires javafx.controls;
    requires com.fasterxml.jackson.databind;

    exports fr.iut.rodez.bubbles;
    exports fr.iut.rodez.bubbles.domain;
    exports fr.iut.rodez.bubbles.fx.graphics;
    exports fr.iut.rodez.bubbles.service to com.fasterxml.jackson.databind;
    exports fr.iut.rodez.bubbles.memento to com.fasterxml.jackson.databind;
    exports fr.iut.rodez.bubbles.fx.model;

    opens fr.iut.rodez.bubbles.domain to com.fasterxml.jackson.databind;
}