package fr.iut.rodez.bubbles.memento;

import fr.iut.rodez.bubbles.domain.Family;

import java.io.Serializable;

public class Memento implements Serializable {
    private static final long serialVersionUID = 1L;
    protected Family state;

    public Memento(Family state) {
        this.state = state.clone();
    }

    public Memento() {}

    public Family getState() {
        return state;
    }

    public void setState(Family pState) {
        state = pState;
    }
}
