package fr.iut.rodez.bubbles.memento;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.iut.rodez.bubbles.service.FamilyService;

import java.io.*;
import java.util.Deque;
import java.util.LinkedList;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

import static com.fasterxml.jackson.databind.PropertyNamingStrategies.SNAKE_CASE;

public class CareTaker {

    private static final ObjectMapper objectMapper;

    static {
        objectMapper = new ObjectMapper();
        objectMapper.setPropertyNamingStrategy(SNAKE_CASE);
    }

    public static final int MEMORY_LIMIT_MAX = 150;
    public static final int MEMORY_LIMIT_MIDDLE = 100;
    public static final int MEMORY_LIMIT_MIN = 50;
    private final Deque<Memento> memoryUndoDeque = new LinkedList<>();
    private final File fileUndoStack = new File("undo_stack.dat");

    public CareTaker() {
    }

    public synchronized void saveState(Memento state) {
        memoryUndoDeque.addLast(state);
        if (memoryUndoDeque.size() >= MEMORY_LIMIT_MAX) {
            saveStatesToFile();
        }
    }

    public synchronized Memento undo() {

        if (memoryUndoDeque.size() <= MEMORY_LIMIT_MIN) {
            loadStatesToFile();
        }
        if (!memoryUndoDeque.isEmpty()) {
            return memoryUndoDeque.removeLast();
        }
        return null;
    }

    private void saveStatesToFile() {

        new Thread(() -> {
            try {
                FamilyService.saveStatesToFileForMemento(fileUndoStack, memoryUndoDeque);
            } catch (IOException e) {
                throw new RuntimeException("Error: ", e);
            }
        }).start();
    }


    private void loadStatesToFile() {

        new Thread(() -> FamilyService.loadStatesToFileForMemento(fileUndoStack, memoryUndoDeque)).start();
    }
}
