package fr.iut.rodez.bubbles;

import fr.iut.rodez.bubbles.domain.Family;
import fr.iut.rodez.bubbles.domain.FamilyMember;
import fr.iut.rodez.bubbles.domain.Relation;
import fr.iut.rodez.bubbles.fx.commons.Paints;
import fr.iut.rodez.bubbles.fx.components.ComputeRelationDialog;
import fr.iut.rodez.bubbles.fx.layout.Layout;
import fr.iut.rodez.bubbles.memento.CareTaker;
import fr.iut.rodez.bubbles.memento.Memento;
import fr.iut.rodez.bubbles.service.FamilyService;
import fr.iut.rodez.bubbles.service.FamilyService.FamilyLoadingResult.Loaded;
import fr.iut.rodez.bubbles.service.FamilyService.FamilyLoadingResult.LoadingError;
import fr.iut.rodez.bubbles.utils.StaticResources;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.TextArea;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

public class Main extends Application {

    private Family loadedFamily;
    private File loadedFile;
    private CareTaker careTaker = new CareTaker();
    private Layout layout;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open Family File...");
        File file = fileChooser.showOpenDialog(primaryStage);
        switch (FamilyService.loadFamilyFromFile(file)) {
        case Loaded loaded -> {
            loadedFamily = loaded.family();
            loadedFile = file;
            onLoaded(primaryStage, loadedFamily);
            careTaker.saveState(loadedFamily.saveToMemento());
        }
        case LoadingError loadingError -> onLoadingError(loadingError.cause());
        }

        primaryStage.setOnCloseRequest(event -> saveFamily());
    }

    private void saveFamily() {
        if (loadedFamily != null && loadedFile != null) {
            try {
                FamilyService.saveFamilyToFile(loadedFamily, loadedFile);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void onLoaded(Stage primaryStage, Family loadedFamily) {
        primaryStage.setTitle(loadedFamily.name());

        layout = new Layout(loadedFamily, careTaker);

        Scene scene = new Scene(layout);

        scene.getStylesheets()
                .add(StaticResources.retrieveResourceURL("/styles.css")
                        .toExternalForm());

        scene.setFill(Paints.BACKGROUND);
        primaryStage.setScene(scene);
        primaryStage.setMaximized(true);
        primaryStage.show();

        // Connect the save functionality to the save button
        if (layout.getBottomAppBar() != null) {
            layout.getBottomAppBar().setOnSaveFamily(this::saveFamily);
            layout.getBottomAppBar().setOnComputeRelation(this::computeRelation);
            layout.getBottomAppBar().setOnUndo(this::undo);
        }
    }

    private void onLoadingError(Exception cause) {
        System.out.println(cause.getMessage());
    }

    private void computeRelation() {
        ObservableList<FamilyMember> members = FXCollections.observableArrayList(loadedFamily.members());
        ComputeRelationDialog dialog = new ComputeRelationDialog(members);
        Optional<ComputeRelationDialog.Result> result = dialog.showAndWait();

        if (result.isPresent()) {
            FamilyMember member1 = result.get().getMember1();
            FamilyMember member2 = result.get().getMember2();

            List<Relation> path = FamilyService.relationBetween(member1, member2);

            StringBuilder message = new StringBuilder();
            if (path.isEmpty()) {
                message.append("No path found between ")
                        .append(member1.getIdentity().name())
                        .append(" and ")
                        .append(member2.getIdentity().name())
                        .append(".");
            } else {
                message.append("Shortest path:\n");
                for (Relation relation : path) {
                    message.append(relation.related().getIdentity().name()).append(" -> ");
                }
                // Remove the last arrow
                if (message.length() > 0) {
                    message.setLength(message.length() - 4);
                }
            }

            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Computed Relations");
            alert.setHeaderText("Shortest Path Between " + member1.getIdentity().name() + " and " + member2.getIdentity().name());
            TextArea textArea = new TextArea(message.toString());
            textArea.setEditable(false);
            alert.getDialogPane().setContent(textArea);
            alert.showAndWait();
        }
    }


    private void undo() {
        Memento memento = careTaker.undo();
        if (memento != null) {
            loadedFamily.restoreFromMemento(memento);
            refreshLayout();
        }
    }

    private void refreshLayout() {
        if (layout != null) {
            layout.refresh(loadedFamily);
        }
    }
}
