package fr.iut.rodez.bubbles.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.iut.rodez.bubbles.domain.*;
import fr.iut.rodez.bubbles.memento.CareTaker;
import fr.iut.rodez.bubbles.memento.Memento;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.*;
import java.util.stream.Collectors;

import static com.fasterxml.jackson.databind.PropertyNamingStrategies.SNAKE_CASE;

public final class FamilyService {

    private static final ObjectMapper objectMapper;

    static {
        objectMapper = new ObjectMapper();
        objectMapper.setPropertyNamingStrategy(SNAKE_CASE);
    }

    private FamilyService() {}

    public static FamilyLoadingResult loadFamilyFromFile(File file) {
        try {
            FamilyFile family = objectMapper.readValue(file, FamilyFile.class);
            Set<FamilyMember> members = loadFamilyMembers(family);
            return new FamilyLoadingResult.Loaded(new Family(family.name(), members));
        } catch (Exception e) {
            return new FamilyLoadingResult.LoadingError(e);
        }
    }

    private static Set<FamilyMember> loadFamilyMembers(FamilyFile familyFile) {
        Set<FamilyMember> loadedMembers = familyFile.members()
                .stream()
                .map(FamilyService::toFamilyMember)
                .collect(Collectors.toSet());

        familyFile.relations()
                .forEach(relation -> createRelation(relation, loadedMembers));

        return loadedMembers;
    }

    private static FamilyMember toFamilyMember(FamilyFile.Member member) {
        FamilyFile.Member.Position fileMemberPosition = member.position();
        Position position = new Position(fileMemberPosition.x(), fileMemberPosition.y());
        Identity identity = new Identity(member.id(), member.name(), member.picture());
        return new FamilyMember(identity, position);
    }

    protected static void createRelation(FamilyFile.Relation relation,
                                 Set<FamilyMember> members) {
        findById(members, relation.from()).ifPresent(from -> findById(members, relation.to()).ifPresent(to -> {
            switch (relation.type()) {
                case PARENT -> from.parentOf(to);
            }
        }));
    }

    protected static Optional<FamilyMember> findById(Set<FamilyMember> members,
                                             UUID id) {
        return members.stream()
                .filter(member -> id.equals(member.getIdentity().id()))
                .findFirst();
    }

    public static List<Relation> relationBetween(FamilyMember source, FamilyMember target) {
        // Queue to manage paths to explore, each element is a list of relations forming a path
        Queue<List<Relation>> queue = new LinkedList<>();

        // Set to keep track of already visited family members
        Set<FamilyMember> visited = new HashSet<>();

        // Initialize the queue with direct relations from the starting member
        for (Relation relation : source.relations()) {
            List<Relation> path = new ArrayList<>();
            path.add(relation);
            queue.add(path);
        }

        // Loop until there are no more paths to explore
        while (!queue.isEmpty()) {
            // Retrieve and remove the oldest path from the queue
            List<Relation> path = queue.poll();
            // Get the last family member in the current path
            FamilyMember lastMember = path.get(path.size() - 1).related();

            // Check if the last member is the target member
            if (lastMember.equals(target)) {
                return path; // Return the found path
            }

            // If the member hasn't been visited, explore their relations
            if (!visited.contains(lastMember)) {
                visited.add(lastMember); // Mark the member as visited
                // For each relation of the last member, create a new path and add it to the queue
                for (Relation nextRelation : lastMember.relations()) {
                    List<Relation> newPath = new ArrayList<>(path);
                    newPath.add(nextRelation);
                    queue.add(newPath);
                }
            }
        }

        // If no path is found, return an empty list
        return Collections.emptyList();
    }

    public static void saveFamilyToFile(Family family, File file) throws IOException {
        FamilyFile familyFile = new FamilyFile(
                family.name(),
                family.members().stream()
                        .map(FamilyService::toFamilyFileMember)
                        .collect(Collectors.toSet()),
                family.members().stream()
                        .flatMap(member -> member.relations().stream()
                                .map(relation -> new FamilyFile.Relation(
                                        member.getIdentity().id(),
                                        relation.related().getIdentity().id(),
                                        FamilyFile.Relation.Type.PARENT
                                ))
                        )
                        .collect(Collectors.toSet())
        );
        objectMapper.writeValue(file, familyFile);
    }

    public static void saveStatesToFileForMemento(Family family, File file) throws IOException {
        FamilyFile familyFile = new FamilyFile(
                family.name(),
                family.members().stream()
                        .map(FamilyService::toFamilyFileMember)
                        .collect(Collectors.toSet()),
                family.members().stream()
                        .flatMap(member -> member.relations().stream()
                                .map(relation -> new FamilyFile.Relation(
                                        member.getIdentity().id(),
                                        relation.related().getIdentity().id(),
                                        FamilyFile.Relation.Type.PARENT
                                ))
                        )
                        .collect(Collectors.toSet())
        );

        try (FileWriter fileWriter = new FileWriter(file, true)) {
            String jsonState = objectMapper.writeValueAsString(familyFile);
            fileWriter.write(jsonState);
            fileWriter.write("\n");
        } catch (IOException e) {
            System.err.println("Erreur lors de l'écriture dans le fichier : " + e.getMessage());
            throw e;
        }
    }

    private static FamilyFile.Member toFamilyFileMember(FamilyMember member) {
        Position position = member.currentPosition();
        return new FamilyFile.Member(
                member.getIdentity().id(),
                member.getIdentity().name(),
                member.getIdentity().picture(),
                new FamilyFile.Member.Position(position.x(), position.y())
        );
    }

    public static void saveStatesToFileForMemento(File fileUndoStack, Deque<Memento> memoryUndoDeque) throws IOException {
        for (int i = 0; i < CareTaker.MEMORY_LIMIT_MIDDLE; i++) {
            if (!memoryUndoDeque.isEmpty()) {
                Family family = memoryUndoDeque.removeFirst().getState();
                saveStatesToFileForMemento(family, fileUndoStack);
            }
        }
    }

    public static void loadStatesToFileForMemento(File fileUndoStack, Deque<Memento> memoryUndoDeque) {
        if (fileUndoStack.exists()) {
            File tempFile = new File("temp_" + fileUndoStack.getName());
            try (RandomAccessFile raf = new RandomAccessFile(fileUndoStack, "r");
                 BufferedWriter writer = new BufferedWriter(new FileWriter(tempFile))) {

                long length = raf.length();
                long position = length;
                StringBuilder line = new StringBuilder();

                // Read the last lines
                while (position > 0 && memoryUndoDeque.size() < CareTaker.MEMORY_LIMIT_MIDDLE) {
                    position--;
                    raf.seek(position);
                    int readByte = raf.readByte();

                    if (readByte == '\n') {
                        if (!line.isEmpty()) {
                            line.reverse();
                            FamilyFile familyFile = objectMapper.readValue(line.toString(), FamilyFile.class);
                            Set<FamilyMember> members = loadFamilyMembers(familyFile);
                            Memento state = new Memento(new Family(familyFile.name(), members));
                            memoryUndoDeque.addFirst(state);
                            line.setLength(0);
                        }
                    } else {
                        line.append((char) readByte);
                    }
                }

                // Capture the last line if end of file is reached and line is not empty
                if (!line.isEmpty() && memoryUndoDeque.size() < CareTaker.MEMORY_LIMIT_MAX) {
                    line.reverse();
                    FamilyFile familyFile = objectMapper.readValue(line.toString(), FamilyFile.class);
                    Set<FamilyMember> members = loadFamilyMembers(familyFile);
                    Memento state = new Memento(new Family(familyFile.name(), members));
                    memoryUndoDeque.addFirst(state);
                }

                // Write the remaining lines to temp file
                raf.seek(0);
                while (position > 0) {
                    int readByte = raf.read();
                    writer.write(readByte);
                    position--;
                }

            } catch (IOException e) {
                e.printStackTrace();
            }

            // Replace the original file with the temp file
            if (fileUndoStack.delete()) {
                tempFile.renameTo(fileUndoStack);
            }
        }
    }

    public sealed interface FamilyLoadingResult {
        record Loaded(Family family) implements FamilyLoadingResult {}

        record LoadingError(Exception cause) implements FamilyLoadingResult {}
    }
}
