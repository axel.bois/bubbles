package fr.iut.rodez.bubbles.fx.components;

import fr.iut.rodez.bubbles.fx.commons.Backgrounds;
import javafx.beans.property.Property;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.layout.BorderPane;

public class Selectable<T> extends BorderPane {

    private final Property<Boolean> selected;

    public Selectable(Node node) {
        super(node);
        setBackground(Backgrounds.TRANSPARENT);
        this.selected = new SimpleBooleanProperty(true);
        selected.addListener((observable, oldValue, newValue) -> {
            if (newValue) {
                setBackground(Backgrounds.GLASS);
            } else {
                setBackground(Backgrounds.TRANSPARENT);
            }
        });
        setPadding(new Insets(8));
    }
}
