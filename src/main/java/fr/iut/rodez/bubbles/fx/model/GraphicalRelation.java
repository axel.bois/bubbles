package fr.iut.rodez.bubbles.fx.model;

import fr.iut.rodez.bubbles.domain.Position;
import fr.iut.rodez.bubbles.fx.graphics.Drawable;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public record GraphicalRelation(
        String emoji,
        String relationName,
        GraphicalFamilyMember source,
        GraphicalFamilyMember target
) implements Drawable {

    @Override
    public void drawWith(GraphicsContext context) {
        context.setStroke(Color.WHITE);
        context.setLineWidth(4.0);
        Position sourcePosition = source.currentPosition();
        Position targetPosition = target.currentPosition();
        context.strokeLine(sourcePosition.x(), sourcePosition.y(), targetPosition.x(), targetPosition.y());

        // Draw the arrowhead
        double arrowLength = 50;
        double arrowWidth = 7;

        double angle = Math.atan2(targetPosition.y() - sourcePosition.y(), targetPosition.x() - sourcePosition.x());

        double sin = Math.sin(angle);
        double cos = Math.cos(angle);

        // Points for the arrowhead
        double x1 = targetPosition.x() - arrowLength * cos + arrowWidth * sin;
        double y1 = targetPosition.y() - arrowLength * sin - arrowWidth * cos;

        double x2 = targetPosition.x() - arrowLength * cos - arrowWidth * sin;
        double y2 = targetPosition.y() - arrowLength * sin + arrowWidth * cos;

        context.setFill(Color.WHITE);
        context.fillPolygon(new double[]{targetPosition.x(), x1, x2}, new double[]{targetPosition.y(), y1, y2}, 3);

        // Draw the emoji
        double midX = (sourcePosition.x() + targetPosition.x()) / 2;
        double midY = (sourcePosition.y() + targetPosition.y()) / 2;

        context.setFill(Color.RED);
        context.setFont(new javafx.scene.text.Font(20));

        context.fillText(emoji, midX, midY);
    }

    @Override
    public boolean covers(Position position) {
        Position sourcePosition = source.currentPosition();
        Position targetPosition = target.currentPosition();
        double x = position.x() - sourcePosition.x();
        double y = position.y() - sourcePosition.y();
        double a = targetPosition.x() - sourcePosition.x();
        double b = targetPosition.y() - sourcePosition.y();
        double t = (x * a + y * b) / (a * a + b * b);
        double xProj = sourcePosition.x() + t * a;
        double yProj = sourcePosition.y() + t * b;
        return position.distanceTo(new Position(xProj, yProj)) <= 4.0;
    }
}
