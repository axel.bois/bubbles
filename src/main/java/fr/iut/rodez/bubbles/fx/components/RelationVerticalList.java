package fr.iut.rodez.bubbles.fx.components;

import fr.iut.rodez.bubbles.fx.commons.Backgrounds;
import fr.iut.rodez.bubbles.fx.model.GraphicalFamily;
import fr.iut.rodez.bubbles.fx.model.GraphicalFamilyMember;
import fr.iut.rodez.bubbles.fx.model.GraphicalRelation;
import javafx.geometry.Pos;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.VBox;

public class RelationVerticalList extends ScrollPane {

    private static final int DEFAULT_SPACING = 16;
    private VBox vbox;

    public RelationVerticalList(GraphicalFamily family) {
        setBackground(Backgrounds.TRANSPARENT);

        vbox = new VBox(DEFAULT_SPACING);
        vbox.setAlignment(Pos.TOP_RIGHT);
        vbox.setBackground(Backgrounds.TRANSPARENT);
        vbox.setFillWidth(false);

        vbox.getChildren()
                .addAll(family.relations.stream()
                        .map(relation -> new RelationDescriptionBox(relation.source(), relation.target(), relation.relationName()))
                        .toList());

        family.relations.addListener((observable, oldValue, newValue) -> {
            vbox.getChildren()
                    .clear();
            for (GraphicalRelation relation : newValue) {
                vbox.getChildren()
                        .add(new RelationDescriptionBox(relation.source(), relation.target(), relation.relationName()));
            }
        });

        setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        setVbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        setFitToWidth(true);
        setContent(vbox);
    }

    public void setGraphicalFamily(GraphicalFamily pGraphicalFamily) {
        vbox.getChildren().clear();
        vbox.getChildren()
                .addAll(pGraphicalFamily.relations.stream()
                .map(relation -> new RelationDescriptionBox(relation.source(), relation.target(), relation.relationName()))
                .toList());

        pGraphicalFamily.relations.addListener((observable, oldValue, newValue) -> {
            vbox.getChildren()
                    .clear();
            for (GraphicalRelation relation : newValue) {
                vbox.getChildren()
                        .add(new RelationDescriptionBox(relation.source(), relation.target(), relation.relationName()));
            }
        });
    }

    public void selectMember(GraphicalFamilyMember member) {
        vbox.getChildren().forEach(node -> {
            if (node instanceof RelationDescriptionBox) {
                RelationDescriptionBox box = (RelationDescriptionBox) node;
                box.setHighlight(box.getMember().equals(member));
            }
        });
    }
}
