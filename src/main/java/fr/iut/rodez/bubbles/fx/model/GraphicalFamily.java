package fr.iut.rodez.bubbles.fx.model;

import fr.iut.rodez.bubbles.domain.Family;
import fr.iut.rodez.bubbles.domain.FamilyMember;
import fr.iut.rodez.bubbles.domain.RelationFactory;
import javafx.beans.property.ListProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;

import java.util.Map;
import java.util.Set;
import java.util.UUID;

import static java.util.stream.Collectors.toMap;

public class GraphicalFamily {

    private final Family family;
    public final ListProperty<GraphicalFamilyMember> members;
    public final ListProperty<GraphicalRelation> relations;

    public GraphicalFamily(Family family) {
        this.family = family;

        Map<UUID, GraphicalFamilyMember> membersById = family.members()
                .stream()
                .collect(toMap(f -> f.identity().id(), GraphicalFamilyMember::new));

        this.members = new SimpleListProperty<>(FXCollections.observableArrayList(membersById.values()));
        this.relations = new SimpleListProperty<>(FXCollections.observableArrayList());

        family.members()
            .forEach(member -> {
                Set<fr.iut.rodez.bubbles.domain.Relation> relations = member.relations();
                relations.forEach(relation -> {
                    GraphicalFamilyMember source = membersById.get(member.getIdentity().id());
                    GraphicalFamilyMember target = membersById.get(relation.related().getIdentity().id());
                    RelationFactory.createRelation(relation, this.relations, source, target);
                });
            });
    }

    public Family getFamily() {
        return family;
    }

    public void addMember(FamilyMember newMember) {
        family.addMember(newMember);
        members.add(new GraphicalFamilyMember(newMember));
    }

    public void removeMember(FamilyMember member) {
        family.removeMember(member);
        members.removeIf(graphicalMember -> {
            return graphicalMember.getFamilyMember().equals(member);
        });
        relations.removeIf(relation -> {
            return relation.source().getFamilyMember().equals(member) || relation.target().getFamilyMember().equals(member);
        });
    }
}
