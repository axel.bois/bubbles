package fr.iut.rodez.bubbles.fx.commons;

import fr.iut.rodez.bubbles.utils.StaticResources;
import javafx.scene.image.Image;

public final class Images {

    public static final Image PERSON = loadIcon("person");
    public static final Image RELATION = loadIcon("relation");
    public static final Image ADD = loadIcon("add");
    public static final Image DELETE = loadIcon("delete");
    public static final Image SAVE = loadIcon("save"); // Ajoutez cette ligne

    private Images() {}

    private static Image loadIcon(String icon) {
        return new Image(StaticResources.loadResourceAsStream("/icons/" + icon + ".png"));
    }
}
