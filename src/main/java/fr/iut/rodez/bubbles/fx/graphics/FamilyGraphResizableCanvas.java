package fr.iut.rodez.bubbles.fx.graphics;

import fr.iut.rodez.bubbles.domain.FamilyMember;
import fr.iut.rodez.bubbles.domain.Identity;
import fr.iut.rodez.bubbles.domain.Position;
import fr.iut.rodez.bubbles.domain.RelationFactory;
import fr.iut.rodez.bubbles.fx.components.RelationVerticalList;
import fr.iut.rodez.bubbles.fx.components.UpdateMemberDialog;
import fr.iut.rodez.bubbles.fx.model.GraphicalFamily;
import fr.iut.rodez.bubbles.fx.model.GraphicalFamilyMember;
import fr.iut.rodez.bubbles.memento.CareTaker;
import fr.iut.rodez.bubbles.memento.Memento;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.geometry.Insets;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceDialog;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;
import java.util.function.Consumer;

import static fr.iut.rodez.bubbles.fx.graphics.FamilyGraphResizableCanvas.EventManagementStrategy.*;

public class FamilyGraphResizableCanvas extends ResizableCanvas {

    private GraphicalFamily family;
    private final List<GraphicalFamilyMember> selectedMembers;
    private final GraphicsContext context;
    private EventManagementStrategy eventManagementStrategy;
    private Position point;
    private final ObjectProperty<GraphicalFamilyMember> selectedMember = new SimpleObjectProperty<>();
    private RelationVerticalList relationList;
    private Consumer<Boolean> onShowActionBox;
    private final CareTaker careTaker;

    public FamilyGraphResizableCanvas(GraphicalFamily family, EventManagementStrategy eventManagementStrategy, CareTaker careTaker) {
        Objects.requireNonNull(family, "family must not be null");
        Objects.requireNonNull(eventManagementStrategy, "event strategy must not be null");

        this.family = family;
        this.eventManagementStrategy = eventManagementStrategy;
        this.selectedMembers = new ArrayList<>();
        this.context = getGraphicsContext2D();
        this.careTaker = careTaker;

        configureCanvasEvents();

        selectedMember.addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                updateRelationListSelection(newValue);
                draw();
                if (onShowActionBox != null) {
                    onShowActionBox.accept(true);
                }
            } else {
                if (onShowActionBox != null) {
                    onShowActionBox.accept(false);
                }
            }
        });
    }

    public void setEventManagementStrategy(EventManagementStrategy eventManagementStrategy) {
        this.eventManagementStrategy = eventManagementStrategy;
    }

    public void setRelationList(RelationVerticalList relationList) {
        this.relationList = relationList;
    }

    public void setOnShowActionBox(Consumer<Boolean> onShowActionBox) {
        this.onShowActionBox = onShowActionBox;
    }

    public GraphicalFamilyMember getSelectedMember() {
        return selectedMember.get();
    }

    public void updateSelectedMember() {
        GraphicalFamilyMember selected = selectedMember.get();
        if (selected != null) {
            UpdateMemberDialog dialog = new UpdateMemberDialog(selected);
            Optional<Identity> updatedIdentity = dialog.showAndWait();
            updatedIdentity.ifPresent(identity -> {
                careTaker.saveState(family.getFamily().saveToMemento()); // Save state before update
                selected.updateIdentity(identity);
                draw();
            });
        }
    }

    public void deleteSelectedMember() {
        GraphicalFamilyMember selected = selectedMember.get();
        if (selected != null) {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Confirmation de suppression");
            alert.setHeaderText("Supprimer le membre de la famille");
            alert.setContentText("Êtes-vous sûr de vouloir supprimer " + selected.getIdentity().name() + " ?");

            Optional<ButtonType> result = alert.showAndWait();
            if (result.isPresent() && result.get() == ButtonType.OK) {
                careTaker.saveState(family.getFamily().saveToMemento()); // Save state before delete
                family.removeMember(selected.getFamilyMember());
                selectedMember.set(null);
                draw();
            }
        }
    }

    private void configureCanvasEvents() {
        setOnMouseDragged(event -> {
            Position position = new Position(event.getX(), event.getY());
            switch (eventManagementStrategy) {
                case NODE_MANIPULATION -> moveSelectedElementsTo(position);
                case EDGE_MANIPULATION -> definePointByReachableElement(position);
            }
            event.consume();
        });

        setOnMousePressed(event -> {
            switch (eventManagementStrategy) {
                case NODE_MANIPULATION, EDGE_MANIPULATION ->
                        selectFirstElementPositionedOn(new Position(event.getX(), event.getY()));
                case ADD_MEMBER -> showAddMemberPopup(new Position(event.getX(), event.getY()));
                case DELETE_MEMBER -> deleteSelectedMember(new Position(event.getX(), event.getY()));
            }
            event.consume();
        });

        setOnMouseReleased(event -> {
            switch (eventManagementStrategy) {
                case EDGE_MANIPULATION -> selectedAsParentsOfPotentialOne(new Position(event.getX(), event.getY()));
                case NODE_MANIPULATION -> {
                    if (!selectedMembers.isEmpty()) {
                        careTaker.saveState(family.getFamily().saveToMemento());
                    }
                }
            }
            event.consume();
        });
    }

    private void showAddMemberPopup(Position position) {
        TextInputDialog dialog = new TextInputDialog();
        dialog.setTitle("Add Family Member");
        dialog.setHeaderText("Enter the details of the new family member");

        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(20, 150, 10, 10));

        TextField name = new TextField();
        name.setPromptText("Name");
        TextField pictureUrl = new TextField();
        pictureUrl.setPromptText("Picture URL");

        grid.add(new Label("Name:"), 0, 0);
        grid.add(name, 1, 0);
        grid.add(new Label("Picture URL:"), 0, 1);
        grid.add(pictureUrl, 1, 1);

        dialog.getDialogPane().setContent(grid);

        Optional<String> result = dialog.showAndWait();
        if (result.isPresent() && !name.getText().isEmpty()) {
            String nameValue = name.getText();
            String pictureUrlValue = pictureUrl.getText();
            try {
                careTaker.saveState(family.getFamily().saveToMemento()); // Save state before add
                addNewFamilyMember(nameValue, pictureUrlValue, position);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }
    }

    private void addNewFamilyMember(String name, String pictureUrl, Position position) throws MalformedURLException {
        URL imageUrl;
        if (pictureUrl.isEmpty()) {
            imageUrl = getClass().getResource("/icons/default.png");
        } else {
            imageUrl = new URL(pictureUrl);
        }
        Identity identity = new Identity(UUID.randomUUID(), name, imageUrl);
        FamilyMember newMember = new FamilyMember(identity, position);
        family.addMember(newMember);
        draw();
    }

    private void deleteSelectedMember(Position position) {
        firstFamilyMemberPositionedOn(position).ifPresent(member -> {
            careTaker.saveState(family.getFamily().saveToMemento()); // Save state before add
            family.removeMember(member.getFamilyMember());
            draw();
        });
    }

    private void moveSelectedElementsTo(Position position) {
        if (!selectedMembers.isEmpty()) {
            selectedMembers.forEach(element -> element.moveTo(position));
            draw();
        }
    }

    private void selectFirstElementPositionedOn(Position position) {
        Optional<GraphicalFamilyMember> optionalMember = firstFamilyMemberPositionedOn(position);
        if (optionalMember.isPresent()) {
            GraphicalFamilyMember member = optionalMember.get();
            if (selectedMember.get() == member) {
                selectedMember.set(null);
            } else {
                selectedMember.set(member);
                selectedMembers.clear();
                selectedMembers.add(member);
            }
            draw();
        }
    }

    private void definePointByReachableElement(Position position) {
        firstFamilyMemberPositionedOn(position).ifPresentOrElse(member -> {
            Position memberPosition = member.currentPosition();
            point = new Position(memberPosition.x(), memberPosition.y());
        }, () -> point = position);
        draw();
    }

    private void selectedAsParentsOfPotentialOne(Position position) {
        String relationTypeString = showAddRelationPopup();
        firstFamilyMemberPositionedOn(position).ifPresent(member -> selectedMembers.forEach(other -> {
            careTaker.saveState(family.getFamily().saveToMemento()); // Save state before add
            RelationFactory.createRelation(relationTypeString, family.relations, other, member);
        }));
        clearSelectionAndPoint();
    }

    private String showAddRelationPopup() {
        List<String> choices = List.of("Parent", "Loved");
        ChoiceDialog<String> dialog = new ChoiceDialog<>("Parent", choices);
        dialog.setTitle("Choose Relation Type");
        dialog.setHeaderText("Select the type of relation");
        dialog.setContentText("Choose your option:");

        return dialog.showAndWait().get();
    }

    private void clearSelectionAndPoint() {
        selectedMembers.clear();
        point = null;
        draw();
    }

    private boolean isTemporaryLineToDraw() {
        return eventManagementStrategy == EDGE_MANIPULATION && point != null && !selectedMembers.isEmpty();
    }

    private Optional<GraphicalFamilyMember> firstFamilyMemberPositionedOn(Position position) {
        return family.members
                .stream()
                .filter(member -> member.covers(position))
                .findFirst();
    }

    @Override
    protected void draw() {
        context.clearRect(0, 0, getWidth(), getHeight());

        family.relations.forEach(relation -> relation.drawWith(context));

        if (isTemporaryLineToDraw() && !selectedMembers.isEmpty()) {
            var firstElement = selectedMembers.getFirst();
            var position = firstElement.currentPosition();
            context.setLineWidth(4.0);
            context.setStroke(Color.GREY);
            context.strokeLine(point.x(), point.y(), position.x(), position.y());
        }

        family.members.forEach(member -> {
            member.setFamilyGraphCanvas(this);
            member.drawWith(context);
        });
    }

    private void updateRelationListSelection(GraphicalFamilyMember selectedMember) {
        if (relationList != null) {
            relationList.selectMember(selectedMember);
        }
    }

    public enum EventManagementStrategy {
        NODE_MANIPULATION, EDGE_MANIPULATION, ADD_MEMBER, DELETE_MEMBER
    }

    public void refresh(GraphicalFamily family) {
        this.family = family;
        draw();
    }

}
