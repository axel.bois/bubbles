package fr.iut.rodez.bubbles.fx.components;

import fr.iut.rodez.bubbles.domain.FamilyMember;
import javafx.collections.ObservableList;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Dialog;
import javafx.scene.control.ButtonType;
import javafx.scene.layout.GridPane;
import javafx.util.StringConverter;

public class ComputeRelationDialog extends Dialog<ComputeRelationDialog.Result> {

    private final ChoiceBox<FamilyMember> member1ChoiceBox;
    private final ChoiceBox<FamilyMember> member2ChoiceBox;

    public ComputeRelationDialog(ObservableList<FamilyMember> members) {
        setTitle("Compute Relation");

        member1ChoiceBox = new ChoiceBox<>(members);
        member2ChoiceBox = new ChoiceBox<>(members);

        // Set the converter to display member names
        StringConverter<FamilyMember> converter = new StringConverter<>() {
            @Override
            public String toString(FamilyMember member) {
                return member != null ? member.getIdentity().name() : "";
            }

            @Override
            public FamilyMember fromString(String string) {
                return members.stream().filter(m -> m.getIdentity().name().equals(string)).findFirst().orElse(null);
            }
        };

        member1ChoiceBox.setConverter(converter);
        member2ChoiceBox.setConverter(converter);

        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.add(member1ChoiceBox, 0, 0);
        grid.add(member2ChoiceBox, 1, 0);

        getDialogPane().setContent(grid);
        getDialogPane().getButtonTypes().addAll(ButtonType.OK, ButtonType.CANCEL);

        setResultConverter(dialogButton -> {
            if (dialogButton == ButtonType.OK) {
                return new Result(member1ChoiceBox.getValue(), member2ChoiceBox.getValue());
            }
            return null;
        });
    }

    public static class Result {
        private final FamilyMember member1;
        private final FamilyMember member2;

        public Result(FamilyMember member1, FamilyMember member2) {
            this.member1 = member1;
            this.member2 = member2;
        }

        public FamilyMember getMember1() {
            return member1;
        }

        public FamilyMember getMember2() {
            return member2;
        }
    }
}
