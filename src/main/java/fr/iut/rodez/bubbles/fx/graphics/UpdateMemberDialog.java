package fr.iut.rodez.bubbles.fx.components;

import fr.iut.rodez.bubbles.domain.Identity;
import fr.iut.rodez.bubbles.fx.model.GraphicalFamilyMember;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.layout.GridPane;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Optional;

public class UpdateMemberDialog {

    private final GraphicalFamilyMember member;

    public UpdateMemberDialog(GraphicalFamilyMember member) {
        this.member = member;
    }

    public Optional<Identity> showAndWait() {
        TextInputDialog dialog = new TextInputDialog();
        dialog.setTitle("Update Family Member");
        dialog.setHeaderText("Update the details of the family member");

        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(20, 150, 10, 10));

        TextField name = new TextField(member.getIdentity().name());
        TextField pictureUrl = new TextField(member.getIdentity().picture().toString());

        grid.add(new Label("Name:"), 0, 0);
        grid.add(name, 1, 0);
        grid.add(new Label("Picture URL:"), 0, 1);
        grid.add(pictureUrl, 1, 1);

        dialog.getDialogPane().setContent(grid);

        Optional<String> result = dialog.showAndWait();
        if (result.isPresent() && !name.getText().isEmpty()) {
            try {
                URL url = new URL(pictureUrl.getText());
                Identity updatedIdentity = new Identity(member.getIdentity().id(), name.getText(), url);
                return Optional.of(updatedIdentity);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }
        return Optional.empty();
    }
}
