package fr.iut.rodez.bubbles.fx.commons;

import javafx.scene.paint.*;

import java.util.List;

public final class Paints {

    public static final Paint GLASS = Color.rgb(255, 255, 255, 0.45);

    private static final Color BACKGROUND_LINEAR_GRADIENT_START = Color.rgb(13, 0, 96);
    private static final Color BACKGROUND_LINEAR_GRADIENT_END = Color.rgb(71, 0, 60);

    public static final Paint BACKGROUND = applicationBackground();

    private Paints() {}

    private static Paint applicationBackground() {
        double startX = 0.0;
        double startY = 0.0;
        double endX = 0.0;
        double endY = 1.0;
        boolean proportional = true;
        return new LinearGradient(
                startX,
                startY,
                endX,
                endY,
                proportional,
                CycleMethod.NO_CYCLE,
                List.of(
                        new Stop(0.0, BACKGROUND_LINEAR_GRADIENT_START),
                        new Stop(1.0, BACKGROUND_LINEAR_GRADIENT_END)
                )
        );
    }
}
