package fr.iut.rodez.bubbles.fx.components;

import javafx.scene.image.Image;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;

import java.net.URL;

public class Avatar extends Circle {

    public Avatar(double radius, URL url) {
        super(radius);
//        Image image = new Image(url.toString());
//        setFill(new ImagePattern(image));
    }

    public Avatar(URL url) {
        this(32, url);
    }
}
