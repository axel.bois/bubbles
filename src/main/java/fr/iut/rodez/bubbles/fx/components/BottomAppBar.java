package fr.iut.rodez.bubbles.fx.components;

import fr.iut.rodez.bubbles.fx.commons.Backgrounds;
import fr.iut.rodez.bubbles.fx.commons.Images;
import fr.iut.rodez.bubbles.fx.graphics.FamilyGraphResizableCanvas;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

import java.util.function.Consumer;

import static fr.iut.rodez.bubbles.fx.graphics.FamilyGraphResizableCanvas.EventManagementStrategy.*;

public class BottomAppBar extends HBox {

    private Consumer<FamilyGraphResizableCanvas.EventManagementStrategy> onEventManagementStrategyChange;
    private Runnable onUpdateMember;
    private Runnable onDeleteMember;
    private Runnable onSaveFamily;
    private Runnable onComputeRelation;
    private Runnable onUndo;
    private VBox actionBox;
    private Label actionLabel;

    public BottomAppBar() {
        setPrefHeight(64);
        setBackground(Backgrounds.GLASS);

        ToggleGroup toggleGroup = new ToggleGroup();

        ToggleButton person = new ToggleButton("Persons", createImageView(Images.PERSON));
        configureButtonSize(person);
        person.setToggleGroup(toggleGroup);

        ToggleButton relation = new ToggleButton("Relations", createImageView(Images.RELATION));
        configureButtonSize(relation);
        relation.setToggleGroup(toggleGroup);

        ToggleButton addMember = new ToggleButton("Add Member", createImageView(Images.ADD));
        configureButtonSize(addMember);
        addMember.setToggleGroup(toggleGroup);

        Button saveButton = new Button("Save", createImageView(Images.SAVE));
        configureButtonSize(saveButton);
        saveButton.setOnAction(event -> {
            if (onSaveFamily != null) {
                onSaveFamily.run();
            }
        });

        Button computeRelationButton = new Button("Compute Relation");
        configureButtonSize(computeRelationButton);
        computeRelationButton.setOnAction(event -> {
            if (onComputeRelation != null) {
                onComputeRelation.run();
            }
        });

        Button undoButton = new Button("Undo");
        configureButtonSize(undoButton);
        undoButton.setOnAction(event -> {
            if (onUndo != null) {
                onUndo.run();
            }
        });

        actionBox = new VBox();
        actionBox.setVisible(false);

        actionLabel = new Label("Actions for");
        Button updateMember = new Button("Update");
        updateMember.setOnAction(event -> {
            if (onUpdateMember != null) {
                onUpdateMember.run();
            }
        });

        Button deleteMember = new Button("Delete");
        deleteMember.setOnAction(event -> {
            if (onDeleteMember != null) {
                onDeleteMember.run();
            }
        });

        actionBox.getChildren().addAll(actionLabel, updateMember, deleteMember);

        toggleGroup.selectToggle(person);

        toggleGroup.selectedToggleProperty()
                .addListener((observable, oldValue, newValue) -> {
                    if (newValue == person) {
                        onEventManagementStrategyChange.accept(NODE_MANIPULATION);
                    } else if (newValue == relation) {
                        onEventManagementStrategyChange.accept(EDGE_MANIPULATION);
                    } else if (newValue == addMember) {
                        onEventManagementStrategyChange.accept(ADD_MEMBER);
                    } else {
                        toggleGroup.selectToggle(oldValue);
                    }
                });

        getChildren().addAll(person, relation, addMember, saveButton, computeRelationButton, undoButton, actionBox);
    }

    private void configureButtonSize(ToggleButton button) {
        button.setMinWidth(100);
        button.setPrefWidth(100);
        button.setMaxWidth(100);
        button.setMinHeight(50);
        button.setPrefHeight(50);
        button.setMaxHeight(50);
    }

    private void configureButtonSize(Button button) {
        button.setMinWidth(100);
        button.setPrefWidth(100);
        button.setMaxWidth(100);
        button.setMinHeight(50);
        button.setPrefHeight(50);
        button.setMaxHeight(50);
    }

    private ImageView createImageView(javafx.scene.image.Image image) {
        ImageView imageView = new ImageView(image);
        imageView.setFitWidth(16);  // Ajustez la largeur de l'image
        imageView.setFitHeight(16); // Ajustez la hauteur de l'image
        return imageView;
    }

    public void setOnEventManagementStrategyChange(Consumer<FamilyGraphResizableCanvas.EventManagementStrategy> onEventManagementStrategyChange) {
        this.onEventManagementStrategyChange = onEventManagementStrategyChange;
    }

    public void setOnUpdateMember(Runnable onUpdateMember) {
        this.onUpdateMember = onUpdateMember;
    }

    public void setOnDeleteMember(Runnable onDeleteMember) {
        this.onDeleteMember = onDeleteMember;
    }

    public void setOnSaveFamily(Runnable onSaveFamily) {
        this.onSaveFamily = onSaveFamily;
    }

    public void setOnComputeRelation(Runnable onComputeRelation) {
        this.onComputeRelation = onComputeRelation;
    }

    public void setOnUndo(Runnable onUndo) {
        this.onUndo = onUndo;
    }

    public void showActionBox(boolean show, String memberName) {
        actionLabel.setText("Actions for " + memberName);
        actionBox.setVisible(show);
    }
}
