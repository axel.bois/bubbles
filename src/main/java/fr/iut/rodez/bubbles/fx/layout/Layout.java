package fr.iut.rodez.bubbles.fx.layout;

import fr.iut.rodez.bubbles.domain.Family;
import fr.iut.rodez.bubbles.memento.CareTaker;
import fr.iut.rodez.bubbles.fx.commons.Backgrounds;
import fr.iut.rodez.bubbles.fx.components.BottomAppBar;
import fr.iut.rodez.bubbles.fx.components.RelationVerticalList;
import fr.iut.rodez.bubbles.fx.graphics.FamilyGraphResizableCanvas;
import fr.iut.rodez.bubbles.fx.model.GraphicalFamily;
import javafx.geometry.Insets;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Region;

public class Layout extends BorderPane {

    private BottomAppBar bottomAppBar;
    private FamilyGraphResizableCanvas graph;
    private GraphicalFamily graphicalFamily;

    private RelationVerticalList relationList;

    public Layout(Family family, CareTaker careTaker) {
        setPadding(new Insets(8));
        setBackground(Backgrounds.TRANSPARENT);

        Pane center = new Pane();

        graphicalFamily = new GraphicalFamily(family);

        relationList = new RelationVerticalList(graphicalFamily);
        graph = new FamilyGraphResizableCanvas(graphicalFamily, FamilyGraphResizableCanvas.EventManagementStrategy.NODE_MANIPULATION, careTaker);

        graph.setRelationList(relationList);

        graph.widthProperty().bind(center.widthProperty());
        graph.heightProperty().bind(center.heightProperty());

        center.getChildren().add(graph);

        bottomAppBar = new BottomAppBar();
        bottomAppBar.setOnEventManagementStrategyChange(graph::setEventManagementStrategy);
        bottomAppBar.setOnUpdateMember(graph::updateSelectedMember);
        bottomAppBar.setOnDeleteMember(graph::deleteSelectedMember);

        graph.setOnShowActionBox(show -> {
            String memberName = graph.getSelectedMember() != null ? graph.getSelectedMember().getIdentity().name() : "";
            bottomAppBar.showActionBox(show, memberName);
        });

        setRight(createContainerFor(relationList));
        setCenter(createContainerFor(center));
        setBottom(createContainerFor(bottomAppBar));
    }

    public Region createContainerFor(Region region) {
        BorderPane pane = new BorderPane(region);
        pane.setPadding(new Insets(8));
        return pane;
    }

    public BottomAppBar getBottomAppBar() {
        return bottomAppBar;
    }

    // Method to refresh the layout with the updated family state
    public void refresh(Family family) {
        graphicalFamily = new GraphicalFamily(family);
        relationList.setGraphicalFamily(graphicalFamily);
        graph.refresh(graphicalFamily);
    }
}
