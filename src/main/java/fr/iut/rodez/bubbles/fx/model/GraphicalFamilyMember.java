package fr.iut.rodez.bubbles.fx.model;

import fr.iut.rodez.bubbles.domain.FamilyMember;
import fr.iut.rodez.bubbles.domain.Identity;
import fr.iut.rodez.bubbles.domain.Position;
import fr.iut.rodez.bubbles.domain.Social;
import fr.iut.rodez.bubbles.fx.graphics.Draggable;
import fr.iut.rodez.bubbles.fx.graphics.FamilyGraphResizableCanvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;

public class GraphicalFamilyMember implements Draggable, Social<GraphicalFamilyMember> {

    private static final double CIRCLE_RADIUS = 32.0;

    private final FamilyMember familyMember;
    private FamilyGraphResizableCanvas familyGraphCanvas;

    public GraphicalFamilyMember(FamilyMember familyMember) {
        this.familyMember = familyMember;
    }

    public void setFamilyGraphCanvas(FamilyGraphResizableCanvas familyGraphCanvas) {
        this.familyGraphCanvas = familyGraphCanvas;
    }

    public Position currentPosition() {
        return familyMember.currentPosition();
    }

    public Identity getIdentity() {
        return familyMember.getIdentity();
    }

    public FamilyMember getFamilyMember() {
        return familyMember;
    }

    @Override
    public void drawWith(GraphicsContext context) {
        Position pos = currentPosition();
        if (isSelected()) {
            context.setFill(Color.RED);
        } else {
            context.setFill(Color.BLACK);
        }
        context.fillOval(pos.x() - CIRCLE_RADIUS, pos.y() - CIRCLE_RADIUS, CIRCLE_RADIUS * 2, CIRCLE_RADIUS * 2);

        context.setFill(Color.WHITE);
        context.setFont(new Font(14));
        context.setTextAlign(TextAlignment.CENTER);
        context.fillText(familyMember.getIdentity().name(), pos.x(), pos.y() + CIRCLE_RADIUS + 15);
    }

    public boolean isSelected() {
        return familyGraphCanvas != null && this.equals(familyGraphCanvas.getSelectedMember());
    }

    public void updateIdentity(Identity newIdentity) {
        familyMember.updateIdentity(newIdentity);
    }

    @Override
    public boolean covers(Position position) {
        return currentPosition().distanceTo(position) <= CIRCLE_RADIUS;
    }

    @Override
    public void moveTo(Position position) {
        familyMember.moveTo(position);
    }

    @Override
    public void parentOf(GraphicalFamilyMember other) {
        familyMember.parentOf(other.familyMember);
    }
    @Override
    public void childOf(GraphicalFamilyMember other) {
        familyMember.childOf(other.familyMember);
    }
    @Override
    public void lovedOf(GraphicalFamilyMember other) {
        familyMember.lovedOf(other.familyMember);
    }
}