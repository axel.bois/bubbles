package fr.iut.rodez.bubbles.domain;

public final class LovedRelation extends Relation {

    public static final String EMOJI = "\u2764";
    public static final String RELATION_NAME = "loved of";

    public LovedRelation(FamilyMember other) {
        super(other);
    }

    @Override
    public LovedRelation clone() {
        return new LovedRelation(related());
    }
}
