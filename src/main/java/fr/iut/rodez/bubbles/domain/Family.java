package fr.iut.rodez.bubbles.domain;

import fr.iut.rodez.bubbles.memento.Memento;

import java.util.HashSet;
import java.util.Set;

public class Family implements Cloneable {

    private final String name;
    private final Set<FamilyMember> members;

    public Family(String name, Set<FamilyMember> members) {
        this.name = name;
        this.members = new HashSet<>(members);
    }

    public String name() {
        return name;
    }

    public Set<FamilyMember> members() {
        return Set.copyOf(members); // defensive copy
    }

    public void addMember(FamilyMember member) {
        members.add(member);
    }

    public void removeMember(FamilyMember member) {
        members.remove(member);
    }

    public Memento saveToMemento() {
        return new Memento(this);
    }

    public void restoreFromMemento(Memento memento) {
        this.members.clear();
        this.members.addAll(memento.getState().members());
    }

    @Override
    public Family clone() {
        Set<FamilyMember> clonedMembers = new HashSet<>();
        for (FamilyMember member : members) {
            clonedMembers.add(member.clone());
        }
        return new Family(this.name, clonedMembers);
    }
}


