package fr.iut.rodez.bubbles.domain;

public interface Social<T> {

    void parentOf(T other);
    void childOf(T other);
    void lovedOf(T other);
}
