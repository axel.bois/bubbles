package fr.iut.rodez.bubbles.domain;

public enum RelationType {
    CHILD,
    PARENT,
    LOVED;
}