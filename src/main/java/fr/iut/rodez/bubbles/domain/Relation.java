package fr.iut.rodez.bubbles.domain;

public sealed abstract class Relation permits ChildRelation, ParentRelation, LovedRelation {

    private final FamilyMember related;

    protected Relation(FamilyMember related) {
        this.related = related;
    }

    public FamilyMember related() {
        return related;
    }

    public abstract Relation clone();
}
