package fr.iut.rodez.bubbles.domain;

import fr.iut.rodez.bubbles.utils.Validations;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FamilyMember implements Social<FamilyMember>, Cloneable {

    private Identity identity;
    private Position position;
    private final Set<Relation> relations;

    public FamilyMember(Identity identity, Position position) {
        this.identity = Objects.requireNonNull(identity, "Identity must not be null.");
        this.position = Objects.requireNonNull(position, "Position must not be null.");
        this.relations = new HashSet<>();
    }

    public Identity identity() {
        return identity;
    }

    public Identity getIdentity() {
        return identity;
    }

    public Set<Relation> relations() {
        return Set.copyOf(relations);
    }

    public Set<FamilyMember> lovedOnes() {
        return lovedOnesAsStream().collect(Collectors.toSet());
    }

    public Set<FamilyMember> children() {
        return relations
                .stream()
                .filter(ChildRelation.class::isInstance)
                .map(Relation::related)
                .collect(Collectors.toSet());
    }

    protected Stream<FamilyMember> lovedOnesAsStream() {
        return relations
                .stream()
                .map(Relation::related);
    }

    public Set<FamilyMember> relatives(int maxDegree) {
        Validations.requireStrictlyPositive(maxDegree, () -> "Max degree must be at least 1.");

        Set<FamilyMember> relatives = new HashSet<>();
        Collection<FamilyMember> familyMembersAtNextDegree = List.of(this);

        for (int degree = 1; degree <= maxDegree; degree++) {

            familyMembersAtNextDegree = familyMembersAtNextDegree
                    .stream()
                    .flatMap(FamilyMember::lovedOnesAsStream)
                    .filter(familyMember -> familyMember != this && !relatives.contains(familyMember))
                    .toList();

            relatives.addAll(familyMembersAtNextDegree);
        }

        return relatives;
    }

    public Position currentPosition() {
        return position;
    }

    public void moveTo(Position position) {
        this.position = position;
    }

    public void updateIdentity(Identity newIdentity) {
        this.identity = newIdentity;
    }

    public boolean isRelated(FamilyMember famMember1) {
        for (Relation rel: famMember1.relations) {
            if (rel.related() == this) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void parentOf(FamilyMember other) {
        relations.add(new ChildRelation(other));
        other.relations.add(new ParentRelation(this));
    }

    @Override
    public void childOf(FamilyMember other) {
        relations.add(new ParentRelation(this));
        other.relations.add(new ChildRelation(other));
    }

    @Override
    public void lovedOf(FamilyMember other) {
        relations.add(new LovedRelation(other));
        other.relations.add(new LovedRelation(this));
    }

    @Override
    public FamilyMember clone() {
        FamilyMember cloned = new FamilyMember(identity, position);
        for (Relation relation : relations) {
            cloned.relations.add(relation.clone());
        }
        return cloned;
    }
}
