package fr.iut.rodez.bubbles.domain;

import fr.iut.rodez.bubbles.fx.commons.Backgrounds;
import fr.iut.rodez.bubbles.fx.components.FamilyMemberBox;
import fr.iut.rodez.bubbles.fx.components.GlassText;
import fr.iut.rodez.bubbles.fx.components.RelationDescriptionBox;
import fr.iut.rodez.bubbles.fx.model.GraphicalFamilyMember;
import fr.iut.rodez.bubbles.fx.model.GraphicalRelation;
import javafx.beans.property.ListProperty;
import javafx.geometry.Pos;
import javafx.scene.shape.Rectangle;

public class RelationFactory {

    public static void createRelation(String relationTypeString, ListProperty<GraphicalRelation> relations, GraphicalFamilyMember source, GraphicalFamilyMember target) {

        RelationType relationType;
        try {
            relationType = RelationType.valueOf(relationTypeString.toUpperCase());
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException("Invalid relation type: " + relationTypeString);
        }

        switch (relationType) {
            case CHILD -> {
                relations.add(new GraphicalRelation(ChildRelation.EMOJI, ChildRelation.RELATION_NAME, source, target));
                source.childOf(target);
            }
            case PARENT -> {
                relations.add(new GraphicalRelation(ParentRelation.EMOJI, ParentRelation.RELATION_NAME, source, target));
                source.parentOf(target);
            }
            case LOVED -> {
                relations.add(new GraphicalRelation(LovedRelation.EMOJI, LovedRelation.RELATION_NAME, source, target));
                source.lovedOf(target);
            }
        }
    }

    public static void createRelation(Relation relation, ListProperty<GraphicalRelation> relations, GraphicalFamilyMember source, GraphicalFamilyMember target) {

        switch (relation) {
            case ChildRelation childRelation -> {
                relations.add(new GraphicalRelation(ChildRelation.EMOJI, ChildRelation.RELATION_NAME, source, target));
                source.childOf(target);
            }
            case ParentRelation parentRelation -> {
                relations.add(new GraphicalRelation(ParentRelation.EMOJI, ParentRelation.RELATION_NAME, source, target));
                source.parentOf(target);
            }
            case LovedRelation lovedRelation -> {
                relations.add(new GraphicalRelation(LovedRelation.EMOJI, LovedRelation.RELATION_NAME, source, target));
                source.lovedOf(target);
            }
            default -> throw new IllegalStateException("Unexpected value: " + relation);
        }
    }
}
