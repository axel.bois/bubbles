package fr.iut.rodez.bubbles.domain;

public final class ParentRelation extends Relation {

    public static final String EMOJI = "\uD83D\uDC6A";
    public static final String RELATION_NAME = "parent of";

    public ParentRelation(FamilyMember other) {
        super(other);
    }

    @Override
    public ParentRelation clone() {
        return new ParentRelation(related());
    }
}
