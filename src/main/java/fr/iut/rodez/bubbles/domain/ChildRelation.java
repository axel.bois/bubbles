package fr.iut.rodez.bubbles.domain;

public final class ChildRelation extends Relation {

    public static final String EMOJI = "\uD83D\uDC6A";
    public static final String RELATION_NAME = "child of";

    public ChildRelation(FamilyMember other) {
        super(other);
    }

    @Override
    public ChildRelation clone() {
        return new ChildRelation(related());
    }
}
