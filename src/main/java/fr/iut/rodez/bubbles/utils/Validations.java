package fr.iut.rodez.bubbles.utils;

import java.util.function.Supplier;

public final class Validations {

    private Validations() {}

    public static double requirePositive(double value, Supplier<String> message) {
        if (Double.isNaN(value) || Double.isInfinite(value) || value < 0.0) {
            throw new IllegalArgumentException(message.get());
        }
        return value;
    }

    public static double requirePositive(double value) {
        return requirePositive(value, () -> "Value must be positive");
    }

    public static int requireStrictlyPositive(int value, Supplier<String> message) {
        if (value <= 0) {
            throw new IllegalArgumentException(message.get());
        }
        return value;
    }

    public static int requireStrictlyPositive(int value) {
        return requireStrictlyPositive(value, () -> "Value must be strictly positive");
    }
}
