# Bubble Application User Manual

Welcome to the Bubble application user manual! Bubble is an intuitive and interactive tool designed to help you create and visualize relationships between individuals. This guide will walk you through all the features of the Bubble application, ensuring you get the most out of your experience.

## Table of Contents

1. [Getting Started](#getting-started)
2. [Creating Individuals](#creating-individuals)
3. [Establishing Relationships](#establishing-relationships)
4. [Viewing Relationship Paths](#viewing-relationship-paths)
5. [Deleting Individuals and Relationships](#deleting-individuals-and-relationships)
6. [Moving Individuals](#moving-individuals)
7. [Tips and Best Practices](#tips-and-best-practices)

## Getting Started

To begin using the Bubble application, open the app and log in with your credentials. Once logged in, you will be greeted by the main interface, where you can start creating your individual profiles and establishing relationships.

All functionalities in the Bubble application are accessed via buttons located in a toolbar at the bottom of the application page.

## Creating Individuals

1. **Add an Individual**: Click on the "Add Individual" button in the bottom toolbar.
2. **Enter Details**: Fill in the name
3. **Name the Individual**: Ensure to give each individual a distinct name for easy identification.

Repeat this process to add more individuals as needed.

## Establishing Relationships

Bubble allows you to create two types of relationships: parent-child and spouse.

1. **Select Relationship**: Click on the relationship button.
2. **Add Relationship**: Click on an individuals and drap the click to another one then a popUP appear, select the type of relationship.
3. **Confirm Relationship**: Click "OK" to save the relationship.

You can repeat this process to create a comprehensive relationship network.

## Viewing Relationship Paths

To understand the connections between individuals:

1. **Select compute relation**: Click on the compute relation button select the two individuals on press "OK".

This feature helps in visualizing the connections and understanding the family tree structure.

## Deleting Individuals and Relationships

1. **Select Delete relation**: Click on the delete button on the botton bar
2. **Delete relation**: Click on relationship you wish to delete.
3. **Delete individuals**: Select an individual and click on the delete button on the bottom bar.

## Moving Individuals

To better visualize the relationship tree, you can move individuals around:

1. **Select Individual**: Click and hold on the individual you want to move.
2. **Drag and Drop**: Drag the individual to the desired location and release the mouse button to drop them.

This feature allows you to arrange the tree in a way that makes the relationships clearer.